const { createCanvas, Image } = require('canvas')

var express = require('express');
var cors = require('cors');
const JSZip = require("jszip");
var router = express.Router();

var app = express();
app.use(express.json());

// the following 2 functions are reused from the meme editor

// Draws one text on the canvas, splitting it into multiple lines if
//  necessary
function drawText(context, lineState) {
  context.font = "bold " + (lineState.fontSize ?? 40) + "px serif";
  if (lineState.color) {
    context.fillStyle = lineState.color;
  }
  let deltaY = 0;
  let lines = lineState.text.split("\n");
  lines.forEach((line) => {
    context.fillText(line, lineState.x, lineState.y + 40 + deltaY,
      lineState.width - 5)// context.measureText("M").width)
    deltaY += 5 + context.measureText(line).actualBoundingBoxAscent
      - context.measureText(line).actualBoundingBoxDescent
  })
}

// Draws all images onto the canvas and returns a Promise, which resolves, when
// all images have been drawn.
//TODO: ensure images are drawn in the correct order (semi-important if they overlap)
function drawImages(context, imagesStates) {
  const imagePromises = imagesStates.map(state => {
    if (state.url) {
      const result = new Promise(resolve => {
        const image = new Image();
        image.onload = () => {
          if (image != null) {
            //draw the image to the canvas context with correct aspect ratio
            const imageAspectRatio = image.width / image.height;
            const canvasAspectRatio = state.width / state.height;
            if (imageAspectRatio > canvasAspectRatio) {
              context.drawImage(image, state.x, state.y, state.width,
                state.width / imageAspectRatio);
            } else {
              context.drawImage(image, state.x, state.y,
                state.height * imageAspectRatio,
                state.height);
            }
            resolve();
          }
        }
        image.onerror = err => {
          console.log(err);
          resolve();
        }
        image.src = state.url;
      });
      return result;
    } else {
      return Promise.resolve();
    }
  });
  return Promise.all(imagePromises);
}

/**
 * Draws the template (given by its id) on the canvas and returns when finished.
 * @param context
 * @param templateId
 * @returns {Promise<unknown>}
 */
function drawTemplate(context, templateId) {
  if(templateId){
    return new Promise(resolve => {
      const image = new Image();
      image.onload = () => {
        if (image != null) {
          context.drawImage(image, 0, 0);
        }
        resolve();
      }
      image.onerror = err => {
        console.log(err);
        resolve();
      }
      image.src = "http://localhost:3001/template/get/" + templateId;
    })
  }

}

/**
 Generates a single meme from a set of texts and a set of images
 Returns the canvas, when everything has been drawn
 */
async function generateSingleMeme(lines, images, canvasWidth, canvasHeight,
    templateId, targetFileSize ) {
  const canvas = createCanvas(canvasWidth, canvasHeight);
  const context = canvas.getContext('2d');

  await drawTemplate(context, templateId);

  await drawImages(context, images);

  if (lines) {
    lines.forEach(line => drawText(context, line));
  }

  let dataURL = canvas.toDataURL();
  let binarySize = (dataURL.length - 22) / 1.3;
  let currentWidth = canvasWidth;
  let currentHeight = canvasHeight;
  if(targetFileSize && binarySize > targetFileSize){
    //Resize if required
    let workingCanvas;
    while (binarySize > targetFileSize) {
      const newImage = new Image();
      await new Promise(resolve => {
        newImage.onload = () => {
          let newWidth = Math.round(currentWidth * 0.9);
          let newHeight = Math.round(currentHeight * 0.9);
          workingCanvas = createCanvas(newWidth, newHeight);
          //workingCanvas.width = newWidth;
          //workingCanvas.height = newHeight;
          const context = workingCanvas.getContext("2d");
          context.drawImage(newImage, 0, 0, newWidth, newHeight);
          dataURL = workingCanvas.toDataURL();
          binarySize = (dataURL.length - 22) / 1.3;
          currentWidth = newWidth;
          currentHeight = newHeight;
          resolve();
        }
        newImage.src = dataURL;
      });
    }
    return workingCanvas;
  }else{
    return canvas;
  }

}

//For CORS-preflight requests
router.options('/', cors());

/**
 Generates a single meme or a set of memes
 Expects in the body a JSON object with the following parameters:
  linesets: a list of lists of text objects, where each text object has the
      properties text, x, y, width, height
      the outer list is for the different memes, each inner list contains all
      texts to draw on a single meme
  images: a list of image objects, each of which needs the following properties:
    x, y, width, height, url (can be a web url, or data-url of an image)
  canvasWidth, canvasHeight: width/height of the canvas(es) to use

 If linesets only contains a single set of texts, a PNG image is returned.
 If linesets contains multiple sets of text, a zip file with multiple memes is
 returned
*/

router.post('/', cors(), async function (req, res, next) {

  const linesets = req.body["linesets"];
  const images = req.body["images"];
  const canvasWidth = req.body["canvasWidth"];
  const canvasHeight = req.body["canvasHeight"];
  const creationDate = req.body["creationDate"]
  const templateId = req.body["templateId"];
  const targetFileSize = req.body["targetFileSize"];


      // If only one set of lines was passed, we generate a single image
  // If multiple sets of text-lines were passed, we generate a zip file of
  // images, each with the same images, but different texts
  if (linesets.length === 1) {
    const stream = (await generateSingleMeme(linesets[0],
        images, canvasWidth, canvasHeight, templateId, targetFileSize)).createPNGStream();
    res.setHeader('Content-Type', 'image/png');

    //saving meme to database
    if(req.body["store"]) {
      fetch('http://localhost:3001/memes/upload', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          canvasWidth: canvasWidth,
          canvasHeight: canvasHeight,
          linesets: linesets,
          images: images,
          user: 'hardcoded test user',
          date: creationDate,
          templateId: templateId,
          targetFileSize: targetFileSize,
        })
      });
    }

    stream.pipe(res);
  } else if (linesets.length > 1) {
    const promises = linesets.map(lines => {
      return generateSingleMeme(lines, images, canvasWidth, canvasHeight,
          templateId, targetFileSize);
    });
    const streams = await Promise.all(promises);
    res.setHeader('Content-Type', 'application/zip');

    const zipFile = new JSZip();
    let counter = 0;
    streams.forEach(stream => {
      zipFile.file("img" + counter++ + ".png",
        Buffer.from(
          stream.toDataURL().replace(/^data:image\/\w+;base64,/, ""),
          "base64"));
    });


    zipFile
      .generateNodeStream({ type: 'nodebuffer', streamFiles: true })
      .pipe(res);

  }
}
)

module.exports = router;
