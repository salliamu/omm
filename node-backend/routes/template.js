const express = require('express');
const router = express.Router();
const fs = require("fs");
const path = require("path");

//For CORS-preflight requests
const cors = require("cors");
router.options('/', cors());

const uploadpath = path.join(__dirname, '../uploads');

function ensureUploadsDirectoryExists() {
  if (!fs.existsSync(uploadpath)) {
    fs.mkdirSync(uploadpath);
  }
}

/**
 Uploads a file as template by storing it in the /uploads directory
 */

router.options("/upload", cors());
router.post('/upload', cors(), async function (req, res, next) {
      ensureUploadsDirectoryExists();
      if (req.files) {
        let file = req.files.file;
        file.mv('./uploads/' + file.name);

        res.send({
          status: true,
          message: 'Template file was successfully uploaded',
          data: {
            name: file.name,
          }
        });
      } else {
        res.send({
          status: false,
          message: 'No template file was passed'
        });
      }
    }
)

/**
 Lists all uploaded templates, i.e. all files in the /uploads directory
 */
router.get('/list', cors(), async function (req, res, next) {
      ensureUploadsDirectoryExists();
      fs.readdir(uploadpath, (error, files) => {
        if (!error) {
          res.send(JSON.stringify(files.map(file => {
            return {id: file, name: file}
          })));
        } else {
          console.log(error);
          res.sendStatus(500);
        }
      })
    }
)

/**
 Retrieves a specific template, given the filename, from the uploads directory
 */
router.get('/get/:id', cors(), async function (req, res, next) {
      ensureUploadsDirectoryExists();
      // root argument ensures that malicious users cannot read files from
      // outside the upload directory
      res.sendFile(req.params.id,
          {root: uploadpath});
    }
)

module.exports = router;