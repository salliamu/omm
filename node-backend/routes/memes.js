var express = require('express');
var router = express.Router();
const cors = require('cors');
const { createCanvas, Image } = require('canvas')
const JSZip = require("jszip");


router.use(cors());


// Get a filtered list of memes ids from the database
router.get('/getAllMemeIds', (req, res) => {
    const db = req.db
    const memes = db.get('memes')
    const filter = req.query
    console.log(filter.page)

    memes.find({ 'user': { $eq: filter.username } }, { fields: '_id upvotecount downvotecount comments views', limit: 4, skip: (filter.page - 1) * 4 })
        //memes.find({ 'user': { $eq: filter.username } },  { fields: '_id'})
        .then(docs => {
            res.json(docs)
        })

        .catch((e) => {
            console.log(e)
            res.status(500).send()
        })
});

//Get one meme image file with the provided id
router.get('/getMeme/:id', async function (req, res, next) {
    const db = req.db
    const memes = db.get('memes')

    console.log(req.params.id)

    memes.findOne({ '_id': req.params.id })
        .then(async (doc) => {
            const linesets = doc.linesets;
            const images = doc.images;
            const canvasWidth = doc.canvasWidth;
            const canvasHeight = doc.canvasHeight;
            const templateId = doc.templateId;
            const targetFileSize = doc.targetFileSize;

            //copied from create.js
            const result = await fetch('http://localhost:3001/generate', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    canvasWidth: canvasWidth,
                    canvasHeight: canvasHeight,
                    linesets: linesets,
                    images: images,
                    user: 'hardcoded test user',
                    store: false,
                    templateId: templateId,
                    targetFileSize: targetFileSize,
                })
            });

            // incrementing view count
            await fetch('http://localhost:3001/memes/view/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ id: req.params.id })
            })

            res.setHeader('Content-Type', result.headers.get('Content-Type'));
            //TODO: check if there is an easier way here
            res.end(Buffer.from(await (await result.blob()).arrayBuffer()));
        }
        )
        .catch((e) => {
            console.log(e);
            res.status(500).send()
        })
})

router.post('/upvote/', async function (req, res, next) {
    const db = req.db
    const memes = db.get('memes')

    const id = req.body.id

    memes.update({ '_id': id }, { $inc: { upvotecount: 1 } })
        .then(docs => {
            res.json(docs)
        })
        .catch((e) => {
            console.log(e)
            res.status(500).send()
        })
})

router.post('/downvote/', async function (req, res, next) {
    const db = req.db
    memes = db.get('memes')

    const id = req.body.id

    memes.update({ '_id': id }, { $inc: { downvotecount: 1 } })
        .then(docs => {
            res.json(docs)
        })
        .catch((e) => {
            console.log(e)
            res.status(500).send()
        })
})

router.post('/comment/', async function (req, res, next) {
    const db = req.db
    const memes = db.get('memes')

    const id = req.body.id
    const username = req.body.username
    const comment = req.body.comment

    memes.update({ '_id': id }, { $push: { "comments": { username: username, comment: comment } } })
        .then(docs => {
            res.json(docs)
        })
        .catch((e) => {
            console.log(e)
            res.status(500).send()
        })
})

router.post('/view/', async function (req, res, next) {
    const db = req.db
    memes = db.get('memes')

    const id = req.body.id

    memes.update({ '_id': id }, { $inc: { views: 1 } })
        .then(docs => {
            res.json(docs)
        })
        .catch((e) => {
            console.log(e)
            res.status(500).send()
        })
})

/**
 * Store a meme in the Database (can be called by the generate-endpoint or
 * directly by the frontend for locally generated memes)
 */
router.post("/upload", (req, res) => {
    const linesets = req.body["linesets"];
    const images = req.body["images"];
    const canvasWidth = req.body["canvasWidth"];
    const canvasHeight = req.body["canvasHeight"];
    const creationDate = req.body["creationDate"]
    const templateId = req.body["templateId"];
    const targetFileSize = req.body["targetFileSize"];


    //saving meme to database
    const db = req.db;
    const collectionMemes = db.get('memes');
    collectionMemes.insert({
        canvasWidth: canvasWidth,
        canvasHeight: canvasHeight,
        linesets: linesets,
        images: images,
        user: "HARDCODED TEST USER",
        date: creationDate,
        templateId: templateId,
        targetFileSize: targetFileSize,
        upvotecount: 0,
        downvotecount: 0,
        comments: [],
        views: 0

    })
        .then((docs) => {
            console.log(docs._id)
        })
        .catch((error) => {
            console.log(error)
        });
    res.end("Success");

});

// /* GET all memes listing. */
// router.get('/', async function (req, res, next) {
//     const db = req.db;
//     const memes = db.get('memes');
//     memes.findOne({})
//         .then(async (doc) => {
//             const linesets = doc.linesets;
//             const images = doc.images
//             const canvasWidth = doc.canvasWidth
//             const canvasHeight = doc.canvasHeight

//             const stream = (await generateSingleMeme(linesets[0],
//                 images, canvasWidth, canvasHeight)).createPNGStream();
//             res.setHeader('Content-Type', 'image/png');
//             stream.pipe(res)
//         }
//         )
//         .catch((e) => {
//             console.log(e);
//             res.status(500).send()
//         })
// });

// router.get('/getAllMemes', async function (req, res, next) {
//     const db = req.db
//     const memes = db.get('memes')

//     memes.find({})
//         .then(async (docs) => {
//             const promises = docs.map(doc => {
//                 return generateSingleMeme(doc.linesets[0], doc.images, doc.canvasWidth, doc.canvasHeight);
//             })

//             const streams = await Promise.all(promises);
//             res.setHeader('Content-Type', 'application/zip');

//             const zipFile = new JSZip();
//             let counter = 0;
//             streams.forEach(stream => {
//                 zipFile.file("img" + counter++ + ".png",
//                     Buffer.from(
//                         stream.toDataURL().replace(/^data:image\/\w+;base64,/, ""),
//                         "base64"));
//             });

//             zipFile
//                 .generateNodeStream({ type: 'nodebuffer', streamFiles: true })
//                 .pipe(res);
//         })

//         .catch((e) => {
//             console.log(e)
//             res.status(500).send()
//         })
// })

// Get a list of all meme ids in database
// Current not used, variant using GET request
// router.get('/getAllMemeIds', async function (req, res, next) {
//     const db = req.db
//     const memes = db.get('memes')

//     memes.find({}, { fields: '_id' })
//         .then(docs => {
//             res.json(docs)
//         })

//         .catch((e) => {
//             console.log(e)
//             res.status(500).send()
//         })
// })

module.exports = router;
