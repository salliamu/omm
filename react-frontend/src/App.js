import React from 'react';
import { useEffect, useState } from 'react';
import { BrowserRouter, Link, Route, Routes, Navigate, Outlet } from "react-router-dom";
import { supabase } from './supabaseClient';

// Components
import Create from "./pages/create";
import Home from "./pages/home";
import Memehistory from "./pages/memehistory"
import Template from "./pages/template"
import Alert from "@mui/material/Alert"
import Login from "./pages/login"
import Signup from "./pages/signup"

// Styles
import './App.css';

function App() {
  // State
  const [session, setSession] = useState(null);

  useEffect(() => {
    supabase.auth.getSession().then(({ data: { session } }) => {
      setSession(session)
    })

    supabase.auth.onAuthStateChange((_event, session) => {
      setSession(session)
    })
  }, []);

  const [alertShown, setAlertShown] = useState(false);
  const [alertContent, setAlertContent] = useState("");

  // For online/offline UI alert
  useEffect(() => {
    window.addEventListener("offline", () => {
      setAlertContent("You are offline! "
        + "You can still view cached content, but cannot upload new templates. "
        + "You can generate memes locally, but cannot upload them")
      setAlertShown(true)

    });
    window.addEventListener("online", async () => {
      setAlertContent("You are online and can enjoy the full experience!")
      await new Promise(r => setTimeout(r, 3000));
      setAlertShown(false)
    })
  }, [])

  const logout = async () => {
    const { error } = await supabase.auth.signOut();
    if (error) {
      console.log(error);
    }
  }

  return (
    <>
      {alertShown ? <Alert variant="outlined" severity="info"
        onClose={() => setAlertShown(false)}>
        {alertContent}
      </Alert> : <></>
      }

      <BrowserRouter>
        {session && (<div className={"navBar"}>
          <Link to="/">Home </Link>
          <Link to="/template"> Add Templates </Link>
          <Link to="/create"> Create Meme </Link>
          <Link to="/memehistory"> Feed </Link>

          <Link className="" to="/" onClick={() => {logout()}}> Logout </Link>
        </div>)}
        
        <div className={"content"}>
          <Routes> 
            <Route path="/login" element={<Login /> }/>
            <Route path="/signup" element={<Signup /> }/>
            <Route element={<ProtectedRoute session={session} />}>
              <Route exact path="/" element={<Home userEmail={session?.user?.userEmail} />} />
              <Route path="/template" element={<Template />} />
              <Route path="/create" element={<Create />} />
              <Route path="/memehistory" element={<Memehistory />} />
            </Route>
          </Routes>
        </div>
      </BrowserRouter>
    </>
  );
}

const ProtectedRoute = ({ session, children }) => {
  if (!session) {
    return <Navigate to="/login" replace />;
  }

  return  children ? children : <Outlet />;
};

export default App;
