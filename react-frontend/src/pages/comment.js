import React, { useEffect, useState } from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Box from "@mui/joy/Box";
import Button from "@mui/joy/Button";
import FormControl from "@mui/joy/FormControl";
import FormLabel from "@mui/joy/FormLabel";
import Textarea from "@mui/joy/Textarea";
import PersonIcon from '@mui/icons-material/Person';

export default function Comments({ image }) {

    const [comments, setComments] = useState([])
    const [textFieldValue, setTextFieldValue] = useState("")

    useEffect(() => {
        setComments(image.comments)
    }, [image])

    const handleChange = (event) => {
        setTextFieldValue(event.target.value);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        comment()
        setTextFieldValue('');
    }

    async function comment(){
        const newComment = { "username": "HARDCODED TEST USER", "comment" : textFieldValue, id: image._id}
        setComments([...comments, newComment])
        console.log(comments)

        const result = await fetch('http://localhost:3001/memes/comment/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(newComment),
        })
        .then((response) => response.json())
        .then((data) => {
            console.log(data)
        })
        .catch((e) => {
            console.log(e)
        })
    }


    return (<>
        {typeof comments !== 'undefined' && comments.length > 0 ?
            <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                {comments.map((comment, index) => {
                    return <React.Fragment key={index}>
                        <ListItem alignItems="flex-start">
                            <ListItemAvatar>
                                <Avatar><PersonIcon/></Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={comment.username} secondary={comment.comment}></ListItemText>
                        </ListItem>
                        <Divider variant="inset" component="li" />
                    </React.Fragment>
                })}
            </List> : <></>}

        <FormControl>
            <FormLabel>Your comment</FormLabel>
            <Textarea
                placeholder="Type something here…"
                minRows={3}
                value={textFieldValue}
                onChange={handleChange}
                endDecorator={
                    <Box
                        sx={{
                            display: "flex",
                            gap: "var(--Textarea-paddingBlock)",
                            pt: "var(--Textarea-paddingBlock)",
                            borderTop: "1px solid",
                            borderColor: "divider",
                            flex: "auto"
                        }}
                    >
                        <Button sx={{ ml: "auto" }} type="submit" onClick={handleSubmit}>Send</Button>
                    </Box>
                }
                sx={{
                    minWidth: 300
                }}
            />
        </FormControl>
    </>
    );
}
