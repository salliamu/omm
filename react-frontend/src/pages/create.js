import {useEffect, useRef, useState} from 'react';
import Button from '@mui/material/Button';
import {
  Alert,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField
} from "@mui/material";
import {Rnd} from "react-rnd";
import "./create.css";

// A TextField to enter the content of a textbox on the canvas
function LineInput(props) {
  return <TextField variant="outlined" className={"control-elem"}
    inputRef={props.ref} onChange={props.onChange}
    label={props.placeholder}
    multiline={true}
    value={props.value} />;
}

// A draggable and resizeable box to show on the canvas. When the user
// drags/resizes this box, the canvas content should update.
function CanvasBox(props) {
  return <Rnd bounds={"parent"}
    className={"drag-box"}
    size={{ width: props.state.width, height: props.state.height }}
    position={{ x: props.state.x, y: props.state.y }}
    onDragStop={(e, d) => {
      props.setState({ ...props.state, x: d.x, y: d.y, });
    }}
    onResizeStop={(e, direction, ref, delta, position) => {
      props.setState({
        ...props.state,
        width: ref.style.width.slice(0, -2),
        height: ref.style.height.slice(0, -2),
        //...position
      });
    }}>
  </Rnd>;
}

/**
 * A toggle button that starts speech recognition and returns the result,
 * if the browser supports it.
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
function SpechRecognitionButton(props) {
  const [recognition, setRecognition] = useState();

  useEffect(()=>{
    if(recognition){
      recognition.continuous = true;
      recognition.onresult = event => {
        if (event.results.length > 0) {
          var current = event.results[0][0]
          var result = current.transcript;
          props.onResult(result);
        }
      }
      recognition.onerror = e => {
        console.log(e);
      }
      recognition.start();
    }
  }, [recognition]);
  return <Button variant={"contained"} component={"label"}
                 className={"control-elem"}
                 disabled={!(window.SpeechRecognition ||
                     window.webkitSpeechRecognition)} onClick={() => {
    if (recognition) {
      recognition.stop();
      setRecognition(null);
    } else {
      const SpeechRecognition = window.SpeechRecognition ||
          window.webkitSpeechRecognition;
      if (SpeechRecognition) {
        setRecognition(new SpeechRecognition());
      }
    }
  }}>
    {recognition ? "Stop" : "Speech input"}
  </Button>;
}

export default function Create() {

  const urlInput = useRef();
  const canvasWidthInput = useRef();
  const canvasHeightInput = useRef();
  const canvas = useRef();
  const resizeCanvas = useRef();
  const uploadQueue = useRef([]);
  const [genTypeText, setGenTypeText] = useState('');
  const [targetFileSize, setTargetFileSize] = useState("");
  const [serverButtonDisabled, setServerButtonDisabled] = useState(false)
  const [alertShown, setAlertShown] = useState(false);
  const [alertContent, setAlertContent] = useState("");

  // Contains the state of all texts on the canvas
  const [linesStates, setLinesStates] = useState({
    "line1": {
      text: "Hello",
      width: 100,
      height: 48,
      x: 10,
      y: 10,
      fontSize: 30,
      color: "red",
    }, "line2": {
      text: "World",
      width: 100,
      height: 48,
      x: 100,
      y: 100,
      fontSize: 30,
      color: "black",
    }
  });
  // Contains the state of all images on the canvas
  const [imagesStates, setImagesStates] = useState([]);
  const [canvasWidth, setCanvasWidth] = useState("400px");
  const [canvasHeight, setCanvasHeight] = useState("400px");
  const [generatedImg, setGeneratedImg] = useState();
  const [templates, setTemplates] = useState([]);
  const [template, setTemplate] = useState("");
  const [templateImg, setTemplateImg] = useState();

  useEffect(() => {
    if (template && template.id) {
      const newImage = new Image();
      newImage.crossOrigin = "anonymous";
      newImage.onerror = (e) => alert("Error loading this template!")
      newImage.onload = () => {
        setTemplateImg(newImage);
        setCanvasHeight(newImage.height+"px");
        setCanvasWidth(newImage.width+"px");
      };
      newImage.src = "http://localhost:3001/template/get/" + template.id
    } else {
      setTemplateImg(null);
    }
  }, [template])

  useEffect(() => {
    redraw();
  }, [canvasWidth, canvasHeight]);

  //canvas state changed
  useEffect(() => {
    redraw();
  }, [linesStates, imagesStates, templateImg]);

  useEffect(() => {
    refreshTemplates();
    window.addEventListener("online", function () {
      while(navigator.onLine && uploadQueue.current.length > 0){
        const uploadBody = uploadQueue.current.shift();
        fetch('http://localhost:3001/memes/upload', {
          method: 'POST',
          headers: {'Content-Type': 'application/json'},
          body: uploadBody
        }).then(() => uploadQueue.current = uploadQueue.current.filter(
            e => e !== uploadBody))
      }
    });
  }, []);

  // Disabling upload to server button when app is offline, giving UI alert
  useEffect(() => {
    window.addEventListener("offline", () => {
      setServerButtonDisabled(true);
    });
    window.addEventListener("online", async () => {
      setServerButtonDisabled(false);
    })
  }, [])

  function refreshTemplates() {
    fetch('http://localhost:3001/template/list').then(result => {
      result.text().then(text => setTemplates([
        {id: null, name: "None"},
        ...JSON.parse(text),
      ]));
    });
  }

  /// Draws all images on the canvas
  function drawImages(context) {
    Object.entries(imagesStates).forEach(([key, state]) => {
      const image = state.image;
      if (image != null) {
        //draw the image to the canvas context with correct aspect ratio
        const imageAspectRatio = image.width / image.height;
        const canvasAspectRatio = state.width / state.height;
        if (imageAspectRatio > canvasAspectRatio) {
          context.drawImage(image, state.x, state.y, state.width,
            state.width / imageAspectRatio);
        } else {
          context.drawImage(image, state.x, state.y,
            state.height * imageAspectRatio,
            state.height);
        }
      }
    });
  }

  /// Draws one text on the canvas, splitting it into multiple lines if
  //  necessary
  function drawText(context, lineState) {
    context.font = "bold " + (lineState.fontSize ?? 40) + "px serif";
    if (lineState.color) {
      context.fillStyle = lineState.color;
    }
    let deltaY = 0;
    let lines = lineState.text.split("\n");
    lines.forEach((line) => {
      context.fillText(line, lineState.x, lineState.y + 40 + deltaY,
        lineState.width - 5)// context.measureText("M").width)
      deltaY += 5 + context.measureText(line).actualBoundingBoxAscent
        - context.measureText(line).actualBoundingBoxDescent
    })
  }

  // Redraws the content of the canvas (images and text)
  function redraw() {
    if (canvas.current) {
      const context = canvas.current.getContext("2d")
      context.clearRect(0, 0, canvas.current.width, canvas.current.height);
      if (templateImg) {
        context.drawImage(templateImg, 0, 0);
      }
      drawImages(context);
      Object.entries(linesStates).forEach(
        ([lineId, lineState]) => drawText(context, lineState));
    }
  }

  /// Loads an image from a url and adds it
  function addUrlImage() {
    // this only allows images with access-control-allow-origin: * headers (e.g. from image hosters)
    // if other images were loaded, the canvas would be tainted and couldn't be
    // exported anymore (that's a security feature implemented by all browsers)
    // TODO: maybe allow "dirty" images or proxy them
    const newImage = new Image();
    newImage.crossOrigin = "anonymous";
    newImage.onerror = (e) => alert("Error loading this image!")
    const url = urlInput.current.value;
    newImage.onload = () => {
      setSingleImageState("img" + (Object.keys(imagesStates).length + 1), {
        image: newImage,
        url: url,
        width: 100,
        height: 48,
        x: 10,
        y: 10
      });
    };
    newImage.src = url
  }

  /// Adds an image to the canvas, which was selected in the file input
  function addFileImage(file) {
    const newImage = new Image();
    newImage.onerror = (e) => alert("Error loading this image!")
    newImage.onload = () => {
      setSingleImageState("img" + (Object.keys(imagesStates).length + 1),
        {
          image: newImage,
          file: file,
          width: 100,
          height: 48,
          x: 10,
          y: 10
        });
    };
    newImage.src = URL.createObjectURL(file);
  }

  /// Downloads the current canvas content as PNG image
  const download = () => {
    const a = document.createElement('a');
    a.download = "exported_file.png";
    a.href = generatedImg;
    a.click();
  }

  /**
   * Returns a JSON object for the generate-meme and upload-meme endpoints
   * @returns {string}
   */
  async function prepareBodyForMemeGenerationAPICall() {
    // For files that were uploaded by the user,
    // generate a data-url-string
    let images = await Promise.all(
        Object.entries(imagesStates).map(([_, state]) => {
          if (state.url) {
            return Promise.resolve(state);
          } else {
            return new Promise(resolve => {
              let reader = new FileReader();
              reader.onload = function () {
                let dataUrl = reader.result;
                resolve({
                  ...state,
                  url: dataUrl,
                });
              };
              reader.readAsDataURL(state.file);
            });
          }
        }));
    return JSON.stringify({
      canvasWidth: parseInt(canvasWidth.replace("px", "")),
      canvasHeight: parseInt(canvasHeight.replace("px", "")),
      linesets: [Object.entries(linesStates).map(
          ([_, lineState]) => lineState)],
      images: images,
      user: 'hardcoded test user',
      creationDate: Date.now(),
      templateId: template.id,
      targetFileSize: targetFileSize,
      store: true,
    });
  }

  /// Generates the meme locally
  const generateLocally = async () => {
    //TODO: upload the image to the server, also if currently offline

    let dataURL = canvas.current.toDataURL();
    let binarySize = (dataURL.length - 22) / 1.3;
    let currentWidth = canvasWidth.slice(0,-2);
    let currentHeight = canvasHeight.slice(0,-2);
    //Resize if required
    //TODO: adjust shown image size
    while (targetFileSize && binarySize > targetFileSize) {
      const newImage = new Image();
      await new Promise(resolve => {
        newImage.onload = () => {
          let newWidth = Math.round(currentWidth * 0.9);
          let newHeight = Math.round(currentHeight * 0.9);
          const workingCanvas = resizeCanvas.current;
          workingCanvas.width = newWidth;
          workingCanvas.height = newHeight;
          const context = workingCanvas.getContext("2d");
          context.drawImage(newImage, 0, 0, newWidth, newHeight);
          dataURL = workingCanvas.toDataURL();
          binarySize = (dataURL.length - 22) / 1.3;
          currentWidth = newWidth;
          currentHeight = newHeight;
          resolve();
        }
        newImage.src = dataURL;
      });
    }
    setGeneratedImg(dataURL);
    setGenTypeText('(generated locally)');

    let uploadBody = await prepareBodyForMemeGenerationAPICall();
    fetch('http://localhost:3001/memes/upload', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: uploadBody
    }).catch(async () => {
      //TODO: maybe store the upload queue globally, such that it works if the user leaves this page
      alert(
          "Meme could not be uploaded to server! If you stay on this page, it"
          + " will be uploaded when you have an internet connection again!");
      uploadQueue.current.push(uploadBody)
    });
  }

  // Generates the meme on the server
  const generateOnServer = async () => {
    setGenTypeText('(generated on server)');
    // call the API endpoint for meme-generation
    fetch('http://localhost:3001/generate', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: await prepareBodyForMemeGenerationAPICall()
    }).then(async result => {
      setGenTypeText('(generated on server)');
      const imgBlob = await result.blob();
      setGeneratedImg(URL.createObjectURL(imgBlob));
    }).catch(()=>alert("Error generating meme!"));
  }

  /// Sets a new state for a single text
  function setSingleLineState(lineId, newLineState) {
    setLinesStates(previousLinesStates => {
      let newState = {
        ...previousLinesStates,
      };
      newState[lineId] = newLineState;
      return newState;
    }
    );
  }

  /// Sets a new state for a single image
  function setSingleImageState(imageId, newImageState) {
    setImagesStates(previousImageStates => {
      let newState = {
        ...previousImageStates,
      };
      newState[imageId] = newImageState;
      return newState;
    }
    );
  }

  return <> 
    <div className={"create-body"}>
      <div>
        <div
          style={{
            width: canvasWidth,
            height: canvasHeight,
            position: "absolute"
          }}>

          {Object.entries(linesStates).map(
            ([lineId, lineState]) => {
              return <CanvasBox state={lineState}
                setState={newLineState =>
                  setSingleLineState(lineId, newLineState)
                }
                key={lineId}

              />;
            })
          }
          {Object.entries(imagesStates).map(
            ([imageId, imageState]) => {
              return <CanvasBox state={imageState}
                setState={newState =>
                  setSingleImageState(imageId, newState)
                }
                key={imageId}

              />;
            })
          }
        </div>


        <canvas ref={canvas}
          width={canvasWidth}
          height={canvasHeight} />
      </div>
      <br /><br />
      <div className={"controls"}>

        <div className={"control-group"}>
          <FormControl sx={{ minWidth: 120 }} className={"control-elem"}>
            <InputLabel>Template</InputLabel>
            <Select
              label="Template"
              value={template.id ?? ""}
              onChange={e => {
                setTemplate(
                  templates.find(template => template.id === e.target.value));
              }
              }
            >
              {templates.map(
                template => <MenuItem value={template.id}
                  key={template.id}>{template.name}</MenuItem>)}
              </Select>
          </FormControl>
          <Button onClick={() => {
            setLinesStates({});
            setImagesStates([]);
          }
          } variant={"contained"}
            className={"control-elem"}>Clear canvas</Button>
        </div>

        <div className={"control-group"}>
          <Button variant={"contained"} component={"label"}
            className={"control-elem"}>
            Upload image
            <input hidden accept={"image/*"} multiple type={"file"}
              onChange={e => addFileImage(e.target.files[0])} />
          </Button>
        </div>

        <div className={"control-group"}>
          <TextField label={"URL of remote image"} inputRef={urlInput}
            variant="outlined" className={"control-elem"} />
          <Button onClick={addUrlImage} variant={"contained"}
            className={"control-elem"}> Load remote
            image </Button>
        </div>
        <br /><br />
        {Object.entries(linesStates).map(
          ([lineId, lineState]) => {
            return <div className={"control-group"}>
              <LineInput
                onChange={e => {
                  setSingleLineState(lineId,
                    { ...lineState, text: e.target.value });
                }}
                placeholder={"Text for " + lineId}
                value={lineState.text} />
              <SpechRecognitionButton onResult={result => {
                setSingleLineState(lineId,
                  { ...lineState, text: result });
              }} />


              <TextField variant="outlined" className={"control-elem"}
                onChange={e => {
                  setSingleLineState(lineId,
                    { ...lineState, fontSize: e.target.value });
                }}
                label={"Font size for " + lineId}
                multiline={false}
                value={lineState.fontSize} />

              <FormControl sx={{ minWidth: 120 }}>
                <InputLabel>{"Text color for " + lineId}</InputLabel>
                <Select
                  onChange={e => {
                    setSingleLineState(lineId,
                      { ...lineState, color: e.target.value });
                  }}
                  label={"Text color for " + lineId}
                  value={lineState.color}
                >
                  {/*TODO: add more colors*/}
                  {["red", "blue", "green", "orange", "black",].map(
                    color => <MenuItem value={color}
                      key={color}>{color}</MenuItem>)}
                </Select>
              </FormControl>
            </div>
          })
        }
        <div className={"control-group"}>
          <Button variant={"contained"} className={"control-elem"} onClick={() =>
            setSingleLineState("line" + (Object.keys(linesStates).length + 1),
              {
                text: "Hello World",
                width: 100,
                height: 48,
                x: 50,
                y: 50,
                fontSize: 30,
                color: "black",
              }
            )}> Add text line </Button>
        </div>

        <div className={"control-group"}>
          <TextField variant="outlined" className={"control-elem"}
            inputRef={canvasWidthInput}
            label={"width (current: " + (canvasWidth?.slice(0, -2)
              ?? "?") + "px)"} />
          <TextField variant="outlined" className={"control-elem"}
            inputRef={canvasHeightInput}
            label={"height (current: " + (canvasHeight?.slice(0, -2)
              ?? "?") + "px)"} />
          <Button variant={"contained"} className={"control-elem"}
            onClick={() => {
              setCanvasWidth(canvasWidthInput.current.value + "px");
              setCanvasHeight(canvasHeightInput.current.value + "px");

              //TODO: when making the canvas smaller,
              // ensure all boxes are still within the canvas' bounds
            }
            }>Set canvas size</Button>
        </div>

        <div className={"control-group"}>
          <TextField variant="outlined" className={"control-elem"}
            onChange={e => {
              setTargetFileSize(e.target.value);
            }}
            label={"max file size in bytes (empty for infinite)"}
            multiline={false}
            value={targetFileSize} />
        </div>

        <div className={"control-group"}>
          <Button variant={"contained"} className={"control-elem"} disabled={serverButtonDisabled}
            onClick={() => generateOnServer()}>
            Generate meme on server</Button>
        </div>

        <div className={"control-group"}>
          <Button variant={"contained"} className={"control-elem"}
            onClick={() => generateLocally()}>
            {generatedImg ? "Regenerate " : "Generate "} meme locally</Button>
        </div>
        <br /><br />


        {generatedImg ? <>
          <h2 className={"control-elem"}>Generated meme {genTypeText}:</h2>
          <img src={generatedImg} className={"generated-img"} style={{
            width: "auto",
            height: "auto",
          }} />
          <br />
          <div className={"control-group"}>
            <Button
              variant={"contained"} className={"control-elem"}
              onClick={() => download()}> Download generated meme </Button>
            <Button
              variant={"contained"} className={"control-elem"}
              onClick={() => alert("not yet implemented")}> TODO: Share
              generated meme </Button>
          </div>
        </>
          : <></>}

      </div>
      <canvas width={400} style={{ visibility: "hidden" }} height={400}
        ref={resizeCanvas} />
    </div>
  </>

}
