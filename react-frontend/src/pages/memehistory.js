import React, { useState, useEffect, useRef, useCallback } from 'react';
import Button from '@mui/material/Button';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import VisibilityIcon from '@mui/icons-material/Visibility';

import Comment from './comment'

import "./memehistory.css";

export default function ImageList() {
    const [images, setImages] = useState([]);
    const [page, setPage] = useState(1);
    const [isFetching, setIsFetching] = useState(false);
    const [hasMore, setHasMore] = useState(true);

    // For inifinite scrolling
    const observer = useRef();
    const lastImageElementRef = useCallback(
        node => {
            if (isFetching) return;
            if (observer.current) observer.current.disconnect();
            observer.current = new IntersectionObserver(entries => {
                if (entries[0].isIntersecting && hasMore) {
                    setPage(page + 1);
                }
            });
            if (node) observer.current.observe(node);
        },
        [isFetching, hasMore]
    );

    // Getting list paginated list with meme ids
    useEffect(() => {
        setIsFetching(true);
        async function fetchData() {
            const allMemesUrl = new URL('http://localhost:3001/memes/getAllMemeIds');
            allMemesUrl.searchParams.append('username', 'HARDCODED TEST USER');
            allMemesUrl.searchParams.append('page', page);

            const result = await fetch(allMemesUrl)
            const data = await result.json()
            if (data.length === 0) {
                setHasMore(false);
            }
            // removing duplicates (weird syntax because data is Prototype.object, which the spread-operator/Set can't deal with)
            setImages(prevImages => {
                const newImages = [...prevImages, ...data];
                const ids = newImages.map(o => o._id)
                return newImages.filter(({ _id }, index) => !ids.includes(_id, index + 1))
            });
            setIsFetching(false);
        }
        fetchData();
    }, [page]);

    async function upvote(id) {
        const result = await fetch('http://localhost:3001/memes/upvote/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({id: id})
        } )
            .then(() => {
                const updatedImages = images.map((image) => {
                    if (image._id == id) {
                        if (image.upvotecount == null) {
                            image.upvotecount = 1
                        } else {
                            image.upvotecount = image.upvotecount + 1
                        }
                        return image;
                    } else {
                        return image;
                    }
                });
                setImages(updatedImages);
            })
            .catch((e) => {
                console.log(e)
            })
    }

    function downvote(id) {
        const result = fetch('http://localhost:3001/memes/downvote/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({id: id})
        })
            .then(() => {
                const updatedImages = images.map((image) => {
                    if (image._id == id) {
                        if (image.downvotecount == null) {
                            image.downvotecount = 1
                        } else {
                            image.downvotecount = image.downvotecount + 1
                        }
                        return image;
                    } else {
                        return image;
                    }
                });
                setImages(updatedImages);
            })
            .catch((e) => {
                console.log(e)
            })
    }

    return (
        <div className='image-container'>

            {images.map((image, index) => {
                if (images.length === index + 1) {
                    return (
                        <React.Fragment key={image._id}>
                            <img
                                ref={lastImageElementRef}
                                key={image._id}
                                src={`http://localhost:3001/memes/getMeme/${image._id}`}
                                alt="image"
                            />
                            <div className='evenly-spaced'>
                                <span>{image.views}</span>
                                <VisibilityIcon />
                                <span>+ {image.upvotecount} </span>
                                <Button onClick={() => upvote(image._id)}><ArrowUpwardIcon /></Button>
                                <span>- {image.downvotecount} </span>
                                <Button onClick={() => downvote(image._id)}><ArrowDownwardIcon /></Button>
                            </div>
                            <Comment image={image}></Comment>
                        </React.Fragment>
                    );
                }
                return <React.Fragment key={image._id}>
                    <img key={image._id} src={`http://localhost:3001/memes/getMeme/${image._id}`} alt="image" />
                    <div className='evenly-spaced'>
                        <span>{image.views}</span>
                        <VisibilityIcon />
                        <span>+ {image.upvotecount} </span>
                        <Button onClick={() => upvote(image._id)}><ArrowUpwardIcon /></Button>
                        <span>- {image.downvotecount} </span>
                        <Button onClick={() => downvote(image._id)}><ArrowDownwardIcon /></Button>
                    </div>
                    <Comment image={image}></Comment>
                </React.Fragment>
            })}
            {isFetching && <div>Loading...</div>}
        </div>
    );
}
