import "./create.css";

import Button from "@mui/material/Button";
import {
  Alert,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField
} from "@mui/material";
import {useEffect, useRef, useState} from "react";

export default function Template() {
  const fileInputRef = useRef();
  const urlInputRef = useRef();
  const canvasRef = useRef();
  const nameInputRef = useRef();
  const isDrawing = useRef(false);
  const context = useRef();
  const [imgflips, setImgflips] = useState([]);
  const [imgflip, setImgflip] = useState();
  const [alertShown, setAlertShown] = useState(false);
  const [alertContent, setAlertContent] = useState("");

  function startDrawing(e) {
    isDrawing.current = true;
    context.current.lineWidth = 3;
    context.current.lineCap = "round";
    context.current.strokeStyle = "black";
    context.current.lineJoin = "round";
    context.current.beginPath();

    const canvasBounds = e.target.getBoundingClientRect();
    context.current.moveTo(e.clientX - canvasBounds.left,
        e.clientY - canvasBounds.top);
  }

  function continueDrawing(e) {
    if (isDrawing.current) {
      const canvasBounds = e.target.getBoundingClientRect();
      context.current.lineTo(e.clientX - canvasBounds.left,
          e.clientY - canvasBounds.top);
      context.current.stroke();
    }
  }

  function endDrawing() {
    context.current.closePath();
    isDrawing.current = false;
  }

  useEffect(() => {
    fetch('https://api.imgflip.com/get_memes').then(result => {
      result.text().then(text => setImgflips(
          JSON.parse(text)["data"]["memes"],
      ));
    });

    context.current = canvasRef.current.getContext("2d");
  }, []);

  async function addFileImage(file) {
    setAlertContent("Uploading...");
    setAlertShown(true);

    const data = new FormData();
    data.append("file", file);
    fetch('http://localhost:3001/template/upload', {
      method: 'POST',
      body: data,
    }).then(async result => {
      setAlertContent((await result.json()).message);
    }).catch(result => {
      setAlertContent("Error")
    });
  }

  async function uploadBlob(value, name) {
    setAlertContent("Uploading...");
    setAlertShown(true);

    const data = new FormData();
    data.append("file",
        new File([await value], name));

    fetch('http://localhost:3001/template/upload', {
      method: 'POST',
      body: data,
      // headers: {
      //   'content-type': file.type,
      //   'content-length': `${file.size}`
      // }
    }).then(async result => {
      setAlertContent((await result.json()).message);
    }).catch(result => {
      setAlertContent("Error");
    });
  }

  async function addUrlImage() {
    fetch(urlInputRef.current.value).then(async value => {
      await uploadBlob(value.blob(), urlInputRef.current.value);
    }).catch(() => alert("Error loading this url!"));
  }

  async function addImgflipImage() {

    fetch(imgflip.url).then(async value => {
      const urlparts = imgflip.url.split(".");
      uploadBlob(value.blob(),
          imgflip.name + "." + urlparts[urlparts.length - 1])
    }).catch(() => alert("Error loading this image"));
  }

  async function addCanvasImage() {
    canvasRef.current.toBlob(value => {
      uploadBlob(value, nameInputRef.current.value + ".png");
    });
  }

  return <div>

    {alertShown ? <Alert variant="outlined" severity="info"
                         onClose={() => setAlertShown(false)}>
      {alertContent}
    </Alert> : <></>
    }

    <div className={"control-group"}>
      <Button variant={"contained"} component={"label"}
              className={"control-elem"}>
        Upload image
        <input hidden accept={"image/*"} type={"file"} ref={fileInputRef}
               onChange={e => addFileImage(e.target.files[0])}/>
      </Button>

    </div>
    <hr/>


    {/*TODO: the capture attribute is only supported on mobile browsers, on desktop
    browsers this shows a regular file input. Check with OMM-Team if this suffices.
    If not, we need to open the camera stream and take a snapshot for this upload method.*/}
    <div className={"control-group"}>
      <Button variant={"contained"} component={"label"}
              className={"control-elem"}>
        Take image
        <input hidden accept={"image/*"} type={"file"} ref={fileInputRef}
               capture={"user"}
               onChange={e => addFileImage(e.target.files[0])}/>
      </Button>

    </div>
    <hr/>


    <div className={"control-group"}>
      <TextField label={"URL of remote image"} inputRef={urlInputRef}
                 variant="outlined" className={"control-elem"}/>
      <Button onClick={addUrlImage} variant={"contained"}
              className={"control-elem"}> Upload as template </Button>
    </div>
    <hr/>

    <div className={"control-group"}>
      <FormControl sx={{minWidth: 160}}>
        <InputLabel>{"imgflip template"}</InputLabel>
        <Select
            onChange={e => {
              setImgflip(e.target.value);
            }}
            label={"imgflip template"}
            value={imgflip ?? ""}
        >
          {imgflips.map(
              imgflip => <MenuItem value={imgflip}
                                   key={imgflip.id}>{imgflip.name}</MenuItem>)}
        </Select>
      </FormControl>
      <Button onClick={addImgflipImage} variant={"contained"}
              className={"control-elem"}> Upload as template </Button>
    </div>
    <hr/>

    <canvas width={"400px"} height={"400px"} ref={canvasRef}
            onMouseDown={startDrawing} onMouseMove={continueDrawing}
            onMouseUp={endDrawing}/>

    <div className={"control-group"}>
      <TextField variant="outlined" className={"control-elem"}
                 inputRef={nameInputRef}
                 label={"Template name"} defaultValue={"draw"}/>
      <Button onClick={addCanvasImage} variant={"contained"}
              className={"control-elem"}> Upload drawn image as
        template </Button>
    </div>



  </div>
      ;
}