const cacheKey = "omm-cache";

self.addEventListener('install', (event) => {
  event.waitUntil(caches.open(cacheKey).then((cache) => {
    return cache.addAll([
      '/favicon.ico', //TODO
      "/",
    ]);
  }));
});

self.addEventListener('fetch', (event) => {
  if (event.request.method === "GET") {
    event.respondWith(caches.open(cacheKey).then((cache) => {
      return fetch(event.request.url).then((response) => {
        cache.put(event.request, response.clone());
        return response;
      }).catch(() => {
        return cache.match(event.request.url);
      });
    }));
  }
});